const eslintrcImportPlugin = {
  plugins: ["import"],
  settings: {
    "import/internal-regex": "^(?:@/|src/)",
    "import/resolver": {
      node: {
        paths: ["."],
      },
    },
  },
  extends: [
    "plugin:import/recommended", // eslint-plugin-import
    "plugin:import/errors", // eslint-plugin-import
    "plugin:import/warnings", // eslint-plugin-import
  ],
  rules: {
    "import/named": "warn",
    "import/export": "warn",
    "import/no-cycle": "error",
    "import/no-relative-parent-imports": "off",
    "import/no-unassigned-import": [
      "error",
      { allow: ["**/*.css", "**/*.png", "**/*.svg", "**/*.jpg", "**/*.jpeg"] },
    ],
    "import/order": [
      "error",
      {
        groups: [
          ["builtin", "external"],
          "internal",
          ["parent", "sibling", "index"],
          "type",
        ],
        pathGroups: [
          {
            pattern: "store/**",
            group: "index",
            position: "after",
          },
          {
            pattern: "ui/**",
            group: "internal",
            position: "after",
          },
          {
            pattern: "lib/**",
            group: "internal",
            position: "after",
          },
          {
            pattern: "types",
            group: "type",
            position: "before",
          },
          {
            pattern: "apis",
            group: "type",
            position: "after",
          },
          {
            pattern: "styles/**",
            group: "type",
            position: "after",
          },
        ],
        pathGroupsExcludedImportTypes: [],
        "newlines-between": "always",
        alphabetize: {
          order: "asc",
          caseInsensitive: true,
        },
        warnOnUnassignedImports: true,
      },
    ],
  },
  typescriptConfig: {
    settings: {
      "import/parsers": {
        "@typescript-eslint/parser": [".ts", ".tsx"],
      },
      "import/resolver": {
        // eslint-import-resolver-typescript
        typescript: {
          project: ".",
          alwaysTryTypes: true,
        },
      },
    },
    extends: [
      "plugin:import/typescript", // Import plugin (eslint-plugin-import)
    ],
  },
};

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
  },
  ignorePatterns: ["node_modules/*", ".next/*", ".out/*", "!.prettierrc.js"], // we want to lint .prettierrc.js (ignored by default by eslint)
  settings: eslintrcImportPlugin.settings,
  plugins: eslintrcImportPlugin.plugins,
  extends: [
    "eslint:recommended", // eslint
    "plugin:react/recommended", // eslint-plugin-react
    "plugin:react-hooks/recommended", // eslint-plugin-react-hooks
    "plugin:tailwind/recommended", // eslint-plugin-tailwind
    "plugin:jsx-a11y/recommended", // eslint-plugin-jsx-a11y
    "plugin:prettier/recommended", // eslint-config-prettier
    ...eslintrcImportPlugin.extends,
  ],
  rules: {
    "react/prop-types": "off", // eslint-plugin-react
    "react/display-name": "error", // eslint-plugin-react
    "react/react-in-jsx-scope": "off", // eslint-plugin-react
    "react-hooks/exhaustive-deps": "warn", // eslint-plugin-react-hooks
    "react-hooks/rules-of-hooks": "error", // eslint-plugin-react-hooks
    "react/jsx-filename-extension": ["error", { extensions: [".tsx"] }], // eslint-plugin-react
    "newline-before-return": "error", // eslint
    "no-unsafe-optional-chaining": "off", // eslint
    "jsx-a11y/anchor-is-valid": "off", // eslint-plugin-jsx-a11y // This rule is not compatible with Next.js's <Link /> components
    "prettier/prettier": ["error", {}, { usePrettierrc: true }], // Includes .prettierrc.js rules
    ...eslintrcImportPlugin.rules,
  },
  overrides: [
    {
      files: ["**/*.ts", "**/*.tsx"],
      plugins: ["@typescript-eslint"],
      parser: "@typescript-eslint/parser",
      settings: eslintrcImportPlugin.typescriptConfig.settings,
      extends: [
        ...eslintrcImportPlugin.typescriptConfig.extends,
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/eslint-recommended",
      ],
      rules: {
        "@typescript-eslint/ban-ts-comment": ["warn"],
        "@typescript-eslint/no-unused-vars": ["error"],
        "@typescript-eslint/no-var-requires": ["error"],
        "@typescript-eslint/explicit-module-boundary-types": ["off"],
        "@typescript-eslint/explicit-function-return-type": ["off"], // I suggest this setting for requiring return types on functions only where useful
      },
    },
  ],
};
