const colors = require("tailwindcss/colors");

module.exports = {
  darkMode: false, // or 'media' or 'class'
  purge: ["./src/**/*.js", "./src/**/*.ts", "./src/**/*.jsx", "./src/**/*.tsx"],
  theme: {
    extend: {
      colors,
    },
  },
  extend: {
    maxWidth: {
      screen: "100vw",
    },
  },
  variants: {
    extend: {
      outline: ["hover", "focus"],
    },
  },
  plugins: [],
};
