import { HColorChecker } from "ui/hybrid";

import type { NextPage } from "next";

const IndexPage: NextPage = () => <HColorChecker />;

export default IndexPage;
