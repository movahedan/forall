import NextHead from "next/head";

import { reduxWrapper } from "store";

import { GBaseLayout } from "ui/layout";

import { webstieName } from "lib/constants";
import { isWebVitalsEnable } from "lib/utils";

import type { AppProps, NextWebVitalsMetric } from "next/app";

import "styles/main.min.css";

const { withRedux } = reduxWrapper;

const App = withRedux(({ Component, pageProps }: AppProps) => (
  <>
    <Head />

    <GBaseLayout>
      <Component {...pageProps} />
    </GBaseLayout>
  </>
));

const Head = () => (
  <NextHead>
    <title>{webstieName}</title>

    <link rel="manifest" href="/manifest.json" />
    <meta name="theme-color" content="#317EFB" />
    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />
    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
    <meta charSet="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=yes, viewport-fit=cover"
    />

    <link
      href="/icons/icon-16x16.png"
      rel="icon"
      type="image/png"
      sizes="16x16"
    />
    <link
      href="/icons/icon-32x32.png"
      rel="icon"
      type="image/png"
      sizes="32x32"
    />
    <link rel="apple-touch-icon" href="/apple-icon.png"></link>

    <link
      href="https://fonts.googleapis.com/css2?family=Inter"
      rel="stylesheet"
    />
  </NextHead>
);

export const reportWebVitals = (metric: NextWebVitalsMetric) => {
  if (isWebVitalsEnable) console.log(metric);
};

export default App; // DO NOT ATTACH a getInitialProps to App
