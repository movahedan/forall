import { useRef, useEffect } from "react";

export function useIsAliveRef() {
  const isAliveRef = useRef(true);

  useEffect(() => {
    isAliveRef.current = true;

    return () => {
      isAliveRef.current = false;
    };
  }, []);

  return isAliveRef;
}
