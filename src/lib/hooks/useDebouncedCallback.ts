import { useCallback, useRef } from "react";

import { useIsAliveRef } from "./useIsAliveRef";

import type { DependencyList } from "react";

export function useDebouncedCallback<T extends (...args: never[]) => unknown>(
  callback: T,
  deps: DependencyList = [],
  debounceMilisecond = 150
) {
  const isAlive = useIsAliveRef();
  const timeout = useRef<NodeJS.Timeout>();

  const debouncedCallback = useCallback<(...args: Parameters<T>) => void>(
    (...args) => {
      if (timeout.current) clearTimeout(timeout.current);

      timeout.current = setTimeout(() => {
        if (isAlive.current) callback(...args);
      }, debounceMilisecond);
    },
    deps
  );

  return debouncedCallback;
}
