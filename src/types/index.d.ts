/* eslint-disable import/no-unassigned-import */
import "@reduxjs/toolkit";
import "react-redux";

import { AppState, AppDispatch } from "store";

declare module "react-redux" {
  export function useSelector<TState = AppState, TSelected = unknown>(
    selector: (state: TState) => TSelected,
    equalityFn?: (left: TSelected, right: TSelected) => boolean
  ): TSelected;

  export function useDispatch<TDispatch = AppDispatch>(): TDispatch;
}
