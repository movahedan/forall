import { FC, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";

import { sampleSliceActions } from "./slice";

export const SampleSliceProvider: FC = () => {
  const timeout = useRef<NodeJS.Timeout | null>(null);
  const dispatch = useDispatch();

  useEffect(() => {
    if (timeout.current) {
      clearInterval(timeout.current);
    }

    timeout.current = setInterval(() => {
      dispatch(sampleSliceActions.tick());
    }, 1000);

    return () => {
      if (timeout.current) {
        clearInterval(timeout.current);
      }
    };
  }, [dispatch]);

  return null;
};
