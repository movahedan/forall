import { ISampleSlice } from "./type";

export const sampleSliceSelector = (state: { sampleSlice: ISampleSlice }) =>
  state.sampleSlice;

export const sampleSliceCountSelector = (state: {
  sampleSlice: ISampleSlice;
}) => sampleSliceSelector(state).count;
