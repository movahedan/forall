import { ISampleSlice } from "./type";

export const sampleSliceInitialState: ISampleSlice = {
  count: 0,
  loading: false,
};
