import { EntityId, EntityState } from "@reduxjs/toolkit";

import { ISample } from "./type";

export const sampleEntitiesSelector = (state: {
  sampleEntity: EntityState<ISample>;
}) => state.sampleEntity;

export const sampleEntitySelector =
  (id: EntityId) => (state: { sampleEntity: EntityState<ISample> }) =>
    sampleEntitiesSelector(state).entities[id];

export const sampleEntityCountSelector =
  (id: EntityId) => (state: { sampleEntity: EntityState<ISample> }) =>
    sampleEntitySelector(id)(state)?.count;
