import { createEntityAdapter } from "@reduxjs/toolkit";

import { ISample } from "./type";

export const sampleEntityAdapter = createEntityAdapter<ISample>({
  selectId: (sampleEntity) => sampleEntity.id,
});
