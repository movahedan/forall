export interface ISample {
  id: string;
  count: number;
}
