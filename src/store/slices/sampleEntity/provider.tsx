import { EntityId } from "@reduxjs/toolkit";
import { FC, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import { sampleEntitiesSelector } from "./selectors";
import { sampleEntityActions } from "./slice";

const SampleEntityProvider: FC<{ id: EntityId }> = ({ id }) => {
  const timeout = useRef<NodeJS.Timeout | null>(null);
  const dispatch = useDispatch();

  useEffect(() => {
    if (timeout.current) {
      clearInterval(timeout.current);
    }

    timeout.current = setInterval(() => {
      dispatch(sampleEntityActions.tick({ id }));
    }, 1000);

    return () => {
      if (timeout.current) {
        clearInterval(timeout.current);
      }
    };
  }, [dispatch, id]);

  return null;
};

export const SampleEntitiesProvider: FC = () => {
  const sampleEntities = useSelector(sampleEntitiesSelector);

  return (
    <>
      {Object.keys(sampleEntities.entities).map((id) => (
        <SampleEntityProvider key={id} id={id} />
      ))}
    </>
  );
};
