import { EntityState } from "@reduxjs/toolkit";

import { ISample, sampleEntity, sampleEntityActions } from "./sampleEntity";
import { ISampleSlice, sampleSlice, sampleSliceActions } from "./sampleSlice";

export type ISlices = {
  sampleSlice: ISampleSlice;
  sampleEntity: EntityState<ISample>;
};

export const slicesReducers = {
  sampleSlice: sampleSlice.reducer,
  sampleEntity: sampleEntity.reducer,
};

export const slicesActions = {
  sampleSlice: sampleSliceActions,
  sampleEntity: sampleEntityActions,
};
