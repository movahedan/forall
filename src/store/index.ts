import { combineReducers, configureStore, Store } from "@reduxjs/toolkit";
import { createWrapper, HYDRATE } from "next-redux-wrapper";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

import { ISlices, slicesReducers } from "./slices";

const store = configureStore<ISlices>({
  reducer: (state, action) => {
    if (action.type === HYDRATE) {
      const hydratedState: ISlices = {
        ...state, // use previous state
        ...action.payload, // apply delta from hydration
      };

      return hydratedState;
    } else {
      const combineReducer = combineReducers<ISlices>(slicesReducers);

      return combineReducer(state, action);
    }
  },
});

export type AppDispatch = typeof store.dispatch;
export type AppState = ReturnType<typeof store.getState>;
export const useStoreSelector: TypedUseSelectorHook<AppState> = useSelector;
export const useStoreDispatch = () => useDispatch<AppDispatch>();

export const reduxWrapper = createWrapper<Store<AppState>>(() => {
  // store.dispatch({ type: HYDRATE, payload: slicesInitialStates });

  return store;
});
