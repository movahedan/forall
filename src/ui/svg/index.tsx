export const SVGLogo = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 1000 1000"
    className={className}
  >
    <circle
      cx="500"
      cy="500"
      r="500"
      transform="rotate(90 500 500)"
      fill="#4EEE7B"
    />
    <circle
      cx="500"
      cy="500"
      r="75"
      transform="rotate(90 500 500)"
      fill="#212121"
    />
    <path d="M499.006 375L500 50" stroke="#212121" strokeWidth="10" />
    <path d="M593.189 406.811L823 177" stroke="#212121" strokeWidth="10" />
    <path d="M500 975V626" stroke="#212121" strokeWidth="10" />
    <path d="M629 500L978 500" stroke="#212121" strokeWidth="10" />
    <path d="M588 588L834.78 834.78" stroke="#212121" strokeWidth="10" />
    <path d="M174 174L420.78 420.78" stroke="#212121" strokeWidth="10" />
    <path d="M163.22 836.78L410 590" stroke="#212121" strokeWidth="10" />
    <path d="M33 500L382 500" stroke="#212121" strokeWidth="10" />
  </svg>
);

export const SVGCheck = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 24 24"
    className={className}
  >
    <path
      fill="currentColor"
      d="M21.4707 7.90646L9.4707 19.9065L3.9707 14.4065L5.3807 12.9965L9.4707 17.0765L20.0607 6.49646L21.4707 7.90646Z"
    />
  </svg>
);

export const SVGClose = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 24 24"
    className={className}
  >
    <path
      fill="currentColor"
      d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z"
    />
  </svg>
);

export const SVGPound = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 24 24"
    className={className}
  >
    <path
      fill="currentColor"
      d="M5.40914 21L6.11914 17H2.11914L2.46914 15H6.46914L7.52914 9H3.52914L3.87914 7H7.87914L8.58914 3H10.5891L9.87914 7H15.8791L16.5891 3H18.5891L17.8791 7H21.8791L21.5291 9H17.5291L16.4691 15H20.4691L20.1191 17H16.1191L15.4091 21H13.4091L14.1191 17H8.11914L7.40914 21H5.40914ZM9.52914 9L8.46914 15H14.4691L15.5291 9H9.52914Z"
    />
  </svg>
);

export const SVGCopy = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 24 24"
    className={className}
  >
    <path
      fill="currentColor"
      d="M19 21H8V7H19V21ZM19 5H8C7.46957 5 6.96086 5.21071 6.58579 5.58579C6.21071 5.96086 6 6.46957 6 7V21C6 21.5304 6.21071 22.0391 6.58579 22.4142C6.96086 22.7893 7.46957 23 8 23H19C19.5304 23 20.0391 22.7893 20.4142 22.4142C20.7893 22.0391 21 21.5304 21 21V7C21 6.46957 20.7893 5.96086 20.4142 5.58579C20.0391 5.21071 19.5304 5 19 5V5ZM16 1H4C3.46957 1 2.96086 1.21071 2.58579 1.58579C2.21071 1.96086 2 2.46957 2 3V17H4V3H16V1Z"
    />
  </svg>
);

export const SVGSwitch = ({ width = 24, height = 24, className = "" }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 24 24"
    className={className}
  >
    <path
      fill="currentColor"
      d="M9 3L5 7H8V14H10V7H13L9 3ZM16 17V10H14V17H11L15 21L19 17H16Z"
    />
  </svg>
);
