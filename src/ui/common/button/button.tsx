import type { MouseEvent, CSSProperties, ReactNode } from "react";

interface IProps {
  description: string;

  role?: string;
  text: ReactNode;

  onClick?: (e: MouseEvent<HTMLButtonElement | HTMLSpanElement>) => void;

  style?: CSSProperties;
  className?: string;
}

export const CButton = ({
  description,
  role,
  text,
  onClick,
  style,
  className,
}: IProps) => {
  if (typeof text === "string" ? !text.trim() : !text)
    throw new Error("Please provide text for your button");
  if (!description.trim())
    throw new Error("Please provide a description for your button");

  const isButton = !role;

  return isButton ? (
    <button
      aria-describedby={description}
      onClick={onClick}
      style={style}
      className={className}
    >
      {text}
    </button>
  ) : (
    <span
      role={role}
      aria-describedby={description}
      style={style}
      className={className}
    >
      {text}
    </span>
  );
};
