import { forwardRef } from "react";

import styles from "./style.module.scss";

import type {
  Ref,
  ReactNode,
  ChangeEvent,
  CSSProperties,
  MouseEventHandler,
} from "react";

type TInputType =
  | "email"
  | "money"
  | "number"
  | "password"
  | "phone"
  | "text"
  | "zip";

interface IProps {
  label: string;
  type: TInputType;
  name?: string;
  required?: boolean;
  tabIndex?: -1 | 0;
  placeholder?: string;
  autocomplete?: "on" | "off";
  value?: number | string;
  prefix?: ReactNode;
  suffix?: ReactNode;
  error?: ReactNode;

  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (e: ChangeEvent<HTMLInputElement>) => void;
  onClick?: MouseEventHandler<HTMLInputElement>;

  style?: CSSProperties;
  className?: string;
}

const Input = (props: IProps, ref: Ref<HTMLInputElement>) => {
  const {
    label,
    type,
    name,
    required,
    tabIndex = 0,
    placeholder = label,
    autocomplete,
    value,
    prefix,
    suffix,
    error,

    onChange,
    onBlur,
    onFocus,
    onClick,

    style,
    className,
  } = props;

  const inputID: string = name || label.toLowerCase();
  const labelText = `${label} ${required ? <span>*</span> : ""}`;

  const wrapperClassName = [
    styles["input-wrapper"],
    !!error && styles["input-wrapper--error"],
    className,
  ]
    .filter(Boolean)
    .join(" ");

  const inputClassName = [
    styles["input-element"],
    !!prefix && styles["input-element--pl-none"],
    !!suffix && styles["input-element--pr-none"],
    className,
  ]
    .filter(Boolean)
    .join(" ");

  return (
    <div style={style} className={wrapperClassName}>
      <label htmlFor={inputID} className={styles["input-label"]}>
        {labelText}
      </label>

      <div className={styles["input-group"]}>
        {!!prefix && (
          <span aria-hidden="true" className={styles["input-prefix"]}>
            {prefix}
          </span>
        )}

        <input
          ref={ref}
          id={inputID}
          type={type}
          name={inputID}
          placeholder={placeholder}
          required={required ?? false}
          tabIndex={tabIndex}
          autoComplete={autocomplete}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          onFocus={onFocus}
          onClick={onClick}
          className={inputClassName}
        />

        {!!suffix && (
          <span aria-hidden="true" className={styles["input-suffix"]}>
            {suffix}
          </span>
        )}
      </div>

      {!!error && (
        <span aria-hidden="true" className={styles["input-error"]}>
          {error}
        </span>
      )}
    </div>
  );
};

export const CInput = forwardRef(Input);
