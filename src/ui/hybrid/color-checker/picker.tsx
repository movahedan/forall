import { useRef, useState, useEffect } from "react";

import { CButton, CInput } from "ui/common";
import { SVGCheck, SVGCopy, SVGPound, SVGSwitch } from "ui/svg";

import { useClipboard, useDebouncedCallback } from "lib/hooks";

import styles from "./style.module.scss";
import { getColorInstance } from "./utils";

import type { Dispatch, MouseEvent, SetStateAction } from "react";

interface IPickerProps {
  defaultPrimary: string;
  defaultSecondary: string;
  setPrimaryColorHex: Dispatch<SetStateAction<string>>;
  setSecondaryColorHex: Dispatch<SetStateAction<string>>;
}

export const Picker = ({
  defaultPrimary,
  defaultSecondary,
  setPrimaryColorHex,
  setSecondaryColorHex,
}: IPickerProps) => {
  const primaryColorInputRef = useRef<HTMLInputElement>(null);
  const [primaryColorInputValue, setPrimaryColorInputValue] =
    useState(defaultPrimary);
  const [primaryColor, setPrimaryColor] = useState(() =>
    getColorInstance(defaultPrimary)
  );
  useEffect(() => {
    setPrimaryColorHex(primaryColor.toHexString());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [primaryColor]);
  const [isCopiedPrimary, copyPrimary] = useClipboard(primaryColorInputValue, {
    successDuration: 5000,
  });

  const secondaryColorInputRef = useRef<HTMLInputElement>(null);
  const [secondaryColorInputValue, setSecondaryColorInputValue] =
    useState(defaultSecondary);
  const [secondaryColor, setSecondaryColor] = useState(() =>
    getColorInstance(defaultSecondary)
  );
  useEffect(() => {
    setSecondaryColorHex(secondaryColor.toHexString());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secondaryColor]);
  const [isCopiedSecondary, copySecondary] = useClipboard(
    secondaryColorInputValue,
    {
      successDuration: 5000,
    }
  );

  const createAndSetColor = (
    color: string,
    colorType: "primary" | "secondary"
  ) => {
    const colorInstance = getColorInstance(color);

    if (colorInstance.isValid()) {
      if (colorType === "primary") setPrimaryColor(colorInstance);
      else if (colorType === "secondary") setSecondaryColor(colorInstance);
    }
  };

  const setColorInputValue = (
    color: string,
    colorType: "primary" | "secondary"
  ) => {
    if (colorType === "primary") setPrimaryColorInputValue(color);
    else if (colorType === "secondary") setSecondaryColorInputValue(color);
  };

  const maskColotTextToHex = (
    color: string,
    colorType?: "primary" | "secondary"
  ) => {
    const colorText = color.replace("#", "");

    if (colorType === "primary" && primaryColorInputRef.current) {
      primaryColorInputRef.current.value = colorText;
    } else if (colorType === "secondary" && secondaryColorInputRef.current) {
      secondaryColorInputRef.current.value = colorText;
    }

    return colorText;
  };

  const onSwap = (e: MouseEvent) => {
    const primaryValue = maskColotTextToHex(secondaryColorInputValue);
    setColorInputValue(primaryValue, "primary");
    createAndSetColor(primaryValue, "primary");

    const secondaryValue = maskColotTextToHex(primaryColorInputValue);
    setColorInputValue(secondaryValue, "secondary");
    createAndSetColor(secondaryValue, "secondary");

    e.stopPropagation();
  };

  const debouncedCreateAndSetColor = useDebouncedCallback(createAndSetColor);

  return (
    <div
      role="presentation"
      onClick={() => primaryColorInputRef.current?.select()}
      style={{ backgroundColor: primaryColor.toHexString() }}
      className={styles["checker-wrapper"]}
    >
      <CInput
        ref={primaryColorInputRef}
        type="text"
        prefix={<SVGPound />}
        autocomplete="off"
        label="Background"
        placeholder="Enter your primary color"
        value={primaryColorInputValue}
        onChange={(e) => {
          const colorText = maskColotTextToHex(e.target.value, "primary");
          setColorInputValue(colorText, "primary");
          debouncedCreateAndSetColor(colorText, "primary");
        }}
        suffix={
          <CButton
            description="Click to copy background value"
            onClick={copyPrimary}
            className="flex items-center justify-center w-full h-full text-xs"
            text={
              isCopiedPrimary ? (
                <SVGCheck className="text-green-500" />
              ) : (
                <SVGCopy />
              )
            }
          />
        }
        error={
          !primaryColorInputValue.length && "Please enter backgroud color!"
        }
        style={{ color: primaryColor.isDark() ? "#fff" : "#000" }}
        className="mx-auto"
      />

      <CButton
        description="Swap the primary and secondaryColor"
        onClick={onSwap}
        style={{ color: primaryColor.isDark() ? "#fff" : "#000" }}
        className={styles["swap-button"]}
        text={<SVGSwitch />}
      />

      <div
        role="presentation"
        onClick={(e) => {
          secondaryColorInputRef.current?.focus();
          secondaryColorInputRef.current?.select();

          e.stopPropagation();
        }}
        style={{
          backgroundColor: secondaryColor.toHexString(),
          color: secondaryColor.isDark() ? "#fff" : "#000",
        }}
        className="flex p-4 mx-auto md:py-6 rounded-md"
      >
        <CInput
          ref={secondaryColorInputRef}
          type="text"
          prefix={<SVGPound />}
          autocomplete="off"
          label="Forground"
          placeholder="000"
          onFocus={(e) => {
            secondaryColorInputRef.current?.select();

            e.stopPropagation();
          }}
          suffix={
            <CButton
              description="Click to copy forground value"
              onClick={copySecondary}
              className="flex items-center justify-center w-full h-full text-xs"
              text={
                isCopiedSecondary ? (
                  <SVGCheck className="text-green-500" />
                ) : (
                  <SVGCopy />
                )
              }
            />
          }
          error={
            !secondaryColorInputValue.length && "Please enter forground color!"
          }
          value={secondaryColorInputValue}
          onChange={(e) => {
            const colorText = maskColotTextToHex(e.target.value, "secondary");
            setColorInputValue(colorText, "secondary");
            debouncedCreateAndSetColor(colorText, "secondary");
          }}
        />
      </div>
    </div>
  );
};
