import { ReactNode, CSSProperties } from "react";

import { SVGCheck, SVGClose } from "ui/svg";

import styles from "./style.module.scss";
import {
  getContrastRatio,
  isReadableLargeAA,
  isReadableLargeAAA,
  isReadableNormalAA,
  isReadableNormalAAA,
} from "./utils";

interface IResultsProps {
  primaryColorHex: string;
  secondaryColorHex: string;
}

export const Results = ({
  primaryColorHex,
  secondaryColorHex,
}: IResultsProps) => {
  if (!primaryColorHex || !secondaryColorHex) return null;

  const readableNormalAA = isReadableNormalAA(
    primaryColorHex,
    secondaryColorHex
  );
  const readableNormalAAA = isReadableNormalAAA(
    primaryColorHex,
    secondaryColorHex
  );
  const readableLargeAA = isReadableLargeAA(primaryColorHex, secondaryColorHex);
  const readableLargeAAA = isReadableLargeAAA(
    primaryColorHex,
    secondaryColorHex
  );
  const contrastRatio = getContrastRatio(primaryColorHex, secondaryColorHex);

  return (
    <ol className="w-full mx-auto mt-12 space-y-6">
      <TestResultItem
        title="What's your colors contrast ratio?"
        resultEntries={[["Contrast Ratio:", `${contrastRatio}:1`]]}
      />
      <TestResultItem
        title="Is readable in normal text?"
        resultEntries={[
          ["AA:", readableNormalAA],
          ["AAA:", readableNormalAAA],
        ]}
      />
      <TestResultItem
        title="Is readable in large text?"
        resultEntries={[
          ["AA:", readableLargeAA],
          ["AAA:", readableLargeAAA],
        ]}
      />
    </ol>
  );
};

interface ITestResultItemProps {
  title?: string;
  resultEntries?: [string, ReactNode][];
  style?: CSSProperties;
  className?: string;
}

const TestResultItem = ({
  title,
  resultEntries,
  style,
  className,
}: ITestResultItemProps) => {
  const shouldRender = !!title || !!resultEntries?.length;
  if (!shouldRender) return null;

  return (
    <li style={style} className={className}>
      <section className="space-y-2">
        {!!title && <p>{title}</p>}
        {!!resultEntries?.length && (
          <ul>
            {resultEntries.map(([key, value]) => (
              <li key={String(key)} className={styles["test-item"]}>
                <span>{String(key)}</span>
                <span>
                  {value === true ? (
                    <SVGCheck className="text-green-500" />
                  ) : value === false ? (
                    <SVGClose className="text-red-700" />
                  ) : (
                    value
                  )}
                </span>
              </li>
            ))}
          </ul>
        )}
      </section>
    </li>
  );
};
