import tinycolor from "tinycolor2";

export const getColorInstance = tinycolor;

export const isReadableNormalAA = (
  primaryColorHex: string,
  secondaryColorHex: string
) =>
  tinycolor.isReadable(primaryColorHex, secondaryColorHex, {
    level: "AA",
    size: "small",
  });

export const isReadableNormalAAA = (
  primaryColorHex: string,
  secondaryColorHex: string
) =>
  tinycolor.isReadable(primaryColorHex, secondaryColorHex, {
    level: "AAA",
    size: "small",
  });

export const isReadableLargeAA = (
  primaryColorHex: string,
  secondaryColorHex: string
) =>
  tinycolor.isReadable(primaryColorHex, secondaryColorHex, {
    level: "AA",
    size: "large",
  });

export const isReadableLargeAAA = (
  primaryColorHex: string,
  secondaryColorHex: string
) =>
  tinycolor.isReadable(primaryColorHex, secondaryColorHex, {
    level: "AAA",
    size: "large",
  });

export const getContrastRatio = (
  primaryColorHex: string,
  secondaryColorHex: string
) => tinycolor.readability(primaryColorHex, secondaryColorHex).toFixed(2);
