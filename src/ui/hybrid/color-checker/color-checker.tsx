import { useState } from "react";

import { SVGLogo } from "ui/svg";

import { Picker } from "./picker";
import { Results } from "./results";
import styles from "./style.module.scss";

const defaultPrimary = "000";
const defaultSecondary = "fff";

export const HColorChecker = () => {
  const [primaryColorHex, setPrimaryColorHex] = useState(defaultPrimary);
  const [secondaryColorHex, setSecondaryColorHex] = useState(defaultSecondary);

  return (
    <div className={styles["color-checker-wrapper"]}>
      <h1 className={styles["color-checker-title"]}>
        <SVGLogo width={40} height={40} className="mr-4" />
        <span>Accessibility Checker</span>
      </h1>

      <Picker
        defaultPrimary={primaryColorHex}
        defaultSecondary={secondaryColorHex}
        setPrimaryColorHex={setPrimaryColorHex}
        setSecondaryColorHex={setSecondaryColorHex}
      />

      <Results
        primaryColorHex={primaryColorHex}
        secondaryColorHex={secondaryColorHex}
      />
    </div>
  );
};
