import { CButton } from "ui/common";

import type { FC } from "react";

export const GButton: FC = () => (
  <CButton text="GButton" description="GButton description" />
);
