const path = require("path");

const connect =
  (...middlewares) =>
  (config) =>
    middlewares[0]
      ? middlewares[0](
          middlewares.slice(1).length ? connect(middlewares.slice(1)) : config
        )
      : config;

const production = "production";
const development = "development";
const devMode = process.env.NODE_ENV != production;

const config = connect()({
  future: {
    webpack5: true,
  },
  mode: devMode ? production : development, // This is used for tree shaking, see the SideEffect declared in package.jsons
  sassOptions: {
    includePaths: [path.join(__dirname, "./src")],
  },
});

module.exports = config;
